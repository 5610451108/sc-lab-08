package Measurable;


public class Data {

	public static double average(Measura[] obj) {
		double sum = 0;
		for (Measura m : obj) {
			sum = sum + m.getMeasure();
		}

		if (obj.length > 0) { return sum / obj.length; } 
		else { return 0; }
	}



	public static Measura min(Measura m1, Measura m2){
		
		if (m1.getMeasure() < m2.getMeasure()){
			
			return m1;
		}
		else{
			
			return m2;
		}
		
	}
}
