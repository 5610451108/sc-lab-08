package Measurable;

public interface Taxable {
	double getTax();
}
