package Measurable;


public class Product implements Taxable{
	public String proname;
	public double proprice;
	
	public Product (String proname,double proprice){
		this.proname = proname;
		this.proprice = proprice;
	}
	
	public double getTax() {
	return proprice*0.07;
	}
	
	public String getProName(){
		return proname;
	}
}
