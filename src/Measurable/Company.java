
package Measurable;



public class Company implements Taxable{
	public String comname;
	public double salary;
	public double expend;
	
	public Company(String comname,double salary,double expend){
		this.comname = comname;
		this.salary = salary;
		this.expend = expend;
	}

	public double getTax() {
		return (salary-expend)*0.3;
	}
	
	public String getName(){
		return comname;
	}
}
